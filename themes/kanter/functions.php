<?php

define('KANTER_THEME_DIRECTORY', esc_url(trailingslashit(get_template_directory_uri())));
define('KANTER_REQUIRE_DIRECTORY', trailingslashit(get_template_directory()));
define('KANTER_DEVELOPMENT', false);

/****************************
    After setup hook
***************************/

add_action('after_setup_theme','kanter_setup_theme');

if(!function_exists('kanter_setup_theme')) {

    function kanter_setup_theme(){

        load_theme_textdomain('kanter', KANTER_THEME_DIRECTORY . 'languages');

        register_nav_menus (array(
            'primary-menu' => esc_html__('Main Navigation', 'kanter'),
            'mobile-menu' => esc_html__('Mobile Navigation', 'kanter'),
            'footer-menu' => esc_html__('Footer Navigation', 'kanter'),
        ));

        add_image_size('kanter_horizont_post', 1170, 658, true);
        add_image_size('kanter_portfolio_popup', 600, 450, true);
        add_image_size('kanter_portfolio_large', 1200, 900, true);
        add_image_size('kanter_service_thumb', 600, 450, true);
        add_image_size( 'kanter_fullsize');

        add_theme_support('title-tag');
        add_theme_support('menus');
        add_theme_support('post-thumbnails');
        add_theme_support('automatic-feed-links');
        add_theme_support('html5', array('comment-list', 'comment-form', 'search-form'));
        add_theme_support('post-formats', array('gallery', 'link', 'quote', 'video', 'audio'));

    }
}
/****************************
    Get options
***************************/
function kanter_get_option( $option, $default='' ){

    if( Kirki::get_option( 'kanter_customize', $option ) ){
        $output = Kirki::get_option( 'kanter_customize', $option );
    }else{
        $output = $default;
    }
    return apply_filters('kanter/kanter_get_option', $output);
}


/****************************
    Preloader color
***************************/
if(!function_exists('kanter_loader_style')) {
    function kanter_loader_style() {
        $output = '';

        $output .= 'style="background-color:'.esc_attr( kanter_get_option('al-preloader-color', '#555') ).'"';

        return $output;
    }
}

/****************************
    Content Width
***************************/
add_action('after_setup_theme', 'kanter_content_width');
if(!function_exists('kanter_content_width')){
    function kanter_content_width() {
        $GLOBALS['content_width'] = apply_filters('kanter_content_width', 1170);
    }
}

/***********************************************************
ACF DEVMODE
 ***********************************************************/

if(!KANTER_DEVELOPMENT) {
    add_filter('acf/settings/show_admin', '__return_false');
    require_once(KANTER_REQUIRE_DIRECTORY . 'framework/includes/custom-fields/custom-fields.php');
}

/****************************
    ACF save JSON
***************************/
add_filter('acf/settings/save_json', 'kanter_acf_save_json');

if(!function_exists('kanter_acf_save_json')) {
    function kanter_acf_save_json($path) {
        $path = KANTER_REQUIRE_DIRECTORY . 'framework/includes/custom-fields';
        return $path;
    }
}

/****************************
    ACF load JSON
***************************/
add_filter('acf/settings/load_json', 'kanter_acf_load_json');

if(!function_exists('kanter_acf_load_json')) {
    function kanter_acf_load_json($paths) {
        unset($paths[0]);
        $paths[] = KANTER_REQUIRE_DIRECTORY . 'framework/includes/custom-fields';
        return $paths;
    }
}

/****************************
    Require Files
***************************/

require_once(KANTER_REQUIRE_DIRECTORY . 'framework/includes/action-config.php');
require_once(KANTER_REQUIRE_DIRECTORY . 'framework/includes/include-config.php');
require_once(KANTER_REQUIRE_DIRECTORY . 'framework/includes/plugin-config.php');
require_once(KANTER_REQUIRE_DIRECTORY . 'framework/includes/woocommerce-config.php');
require_once(KANTER_REQUIRE_DIRECTORY . 'framework/includes/helper-function.php');
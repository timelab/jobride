<?php

    if ( post_password_required() ) {
        return;
    }

?>

<?php if(have_comments()) : ?>

    <!-- Comments start -->
    <div class="al-comments">
        <div class="al-comments-number">
            <?php comments_number(esc_html__('No Comments', 'kanter'), esc_html__('One Comment', 'kanter'), esc_html__('% Comments', 'kanter')); ?>
        </div>


        <?php if(get_comment_pages_count() > 1 && get_option('page_comments')): ?>
            <nav class="al-comments-navigation">
                <ul>
                    <li><?php previous_comments_link( esc_html__( 'Older Comments', 'kanter' ) ); ?></li>
                    <li><?php next_comments_link( esc_html__( 'Newer Comments', 'kanter' ) ); ?></li>
                </ul>
            </nav>
         <?php endif; ?>

        <ul class="al-comments-list">
            <?php wp_list_comments( 'type=comment&callback=kanter_comments' ); ?>
        </ul>
    </div>
    <!-- Comments end -->
<?php endif; ?>

<?php if(!comments_open() && get_comments_number() && post_type_supports( get_post_type(), 'comments' )) : ?>
    <div class="al-comments">
        <p class="al-no-comments">
            <?php echo esc_html__('Comments are closed', 'kanter') ?>
        </p>
    </div>
<?php endif; ?>

<?php if( comments_open() ) : ?>

    <?php
    $fields = array(

        'author'  =>
            '<div class="row">',
            '<div class="col-md-4">',
            '<input  id="name" required="required" placeholder="Your name *" name="author" type="text" value="' . esc_attr(  $commenter['comment_author'] ) .
            '" size="30" />',
        '</div>',

        'email'  =>
            '<div class="col-md-4">',
            '<input  id="email" required="required" placeholder="Email *" name="email" type="text" value="' . esc_attr(  $commenter['comment_author_email'] ) .
            '" size="30" />',
        '</div>',

        'url'  =>
            '<div class="col-md-4">',
            '<input  id="url" placeholder="Web site" name="url" type="text" value="' . esc_attr(  $commenter['comment_author_url'] ) .
            '" size="30" />',
        '</div>',
        '</div>',
    );
    $fields = apply_filters('comment_form_default_fields', $fields);
    $args = array(
        'fields'               => $fields,
        'comment_field'        => '<textarea id="comment" name="comment"  placeholder="Comment *" cols="3" rows="5" required="required"></textarea>',
        'logged_in_as'         => '',
        'id_form'              => 'form-comment',
        'id_submit'            => 'submit',
        'class_form'           => 'al-comment-form',
        'class_submit'         => 'com_submits',
        'name_submit'          => 'submit',
        'title_reply'          => esc_html__( 'Leave a Reply' , 'kanter'),
        'title_reply_to'       => esc_html__( 'Leave a Reply to %s' , 'kanter'),
        'title_reply_before'   => '<div class="al-comments-title"><h4>',
        'title_reply_after'    => '</h4></div>',
        'cancel_reply_before'  => ' <small>',
        'cancel_reply_after'   => '</small>',
        'cancel_reply_link'    => esc_html__( 'Cancel reply' , 'kanter'),
        'label_submit'         => esc_html__( 'Submit Reply' , 'kanter'),
        'submit_button'        => '<input name="%1$s" type="submit" id="%2$s" class="btn accent-btn" value="Post comment" />',
        'submit_field'         => '%1$s %2$s',
    );
    comment_form($args);
    ?>

<?php endif; ?>

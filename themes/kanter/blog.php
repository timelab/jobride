<?php

    /* Template Name: Blog Template */

    $blog_title = kanter_get_option('al-blog-heading', 'Blog');
    $blog_description = get_bloginfo('description');

    if( kanter_get_option('al-blog-description') ){
        $blog_description = kanter_get_option('al-blog-description');
    }

    if(class_exists('acf')){
        if(!empty(get_field('blog_title'))){
            $blog_title = get_field('blog_title');
        }
        if(!empty(get_field('blog_description'))){
            $blog_description = get_field('blog_description');
        }
    }
?>

<?php get_header(); ?>

<section <?php echo kanter_blog_background(); ?> class="display-page al-bg-mask background-image">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="content">
                    <!-- heading title-->
                    <h1><?php echo esc_html($blog_title); ?></h1>

                    <span class="horizontal-line"></span><span></span>
                    <!-- description slider-->
                    <div class="description">
                        <p><?php echo esc_html($blog_description); ?></p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<div class="blog">
    <?php if(class_exists('acf')): ?>
        <?php get_template_part('loop/loop-post','acf'); ?>
    <?php endif; ?>
</div>


<?php get_footer(); ?>

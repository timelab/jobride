<?php

    get_header();

    global $post;

    $paged = get_query_var('paged') ? get_query_var('paged') : (get_query_var('page') ? get_query_var('page') : 1);
//    $services_max_posts = $items_per;

    $args = array(
        'paged' => $paged,
//        'posts_per_page' => $services_max_posts,
        'post_type' => 'service'
    );

    $all_services = new WP_Query($args);

    if(class_exists('acf')) {
        $small_description = get_field('service_small_description');
        $icon = get_field('service_icon');
        $service_supported = get_field('service_supported_list');
        $small_title = get_field('service_small_title');
        $description = get_field('service_description');

        if(empty($service_supported)){
            $content_class = 'col-md-12';
        }
    }

?>





<section class="service-single">
    <div class="container">
        <div class="row al-service-single">
            <div class="col-md-3 al-services-nav">
                <ul class="al-services-list">
                    <?php
                        while ($all_services->have_posts()) : $all_services->the_post();
                            echo '<li><a href="'.get_the_permalink().'" title="'.get_the_title().'">'.get_the_title().'</a></li>';
                        endwhile;
                    ?>
                </ul>
            </div>
            <?php while ( have_posts() ) : the_post() ?>


            <div class="al-services-content col-md-9">
                <div class="content-service-single">
                    <h4 class="al-headitg-title"><?php echo get_the_title(); ?></h4>
                    <?php echo wp_kses_post($description); ?>
                </div>


                    <?php if (has_post_thumbnail()) { ?>
                       <div class="al-service-thumb">
                           <div class="post-thumb al-responsive-img">
                               <?php if (function_exists('add_theme_support'))
                                   the_post_thumbnail('kanter_service_thumb'); ?>
                           </div>
                       </div>
                    <?php } ?>


                <?php if(!empty($service_supported)): ?>
                    <ul class="list">
                        <?php
                        foreach ($service_supported as $val) {
                            echo '<li>'.$val['service_supported_list_item'].'</li>';
                        }
                        ?>
                    </ul>
                <?php endif; ?>

            </div>

            <?php endwhile; ?>


        </div>
    </div>
</section>

<div class="container al-side-container">
    <?php while ( have_posts() ) : the_post() ?>
        <?php the_content(); ?>
    <?php endwhile; ?>
</div>

<?php if ( comments_open() || get_comments_number() ) : ?>
    <div class="al-comment-any-page">
        <div class="container">
            <div class="row">
                <?php comments_template(); ?>
            </div>
        </div>
    </div>
<?php endif; ?>


<?php get_footer(); ?>
<?php get_header(); ?>

<section <?php echo kanter_blog_background(); ?> class="display-page al-bg-mask background-image">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="content">
                    <!-- heading title-->
                    <h1><?php echo esc_html(kanter_get_option('al-blog-heading', 'Blog')); ?></h1>
                    <!-- horizontal line-->
                    <span class="horizontal-line"></span><span></span>
                    <!-- description slider-->
                    <div class="description">
                        <?php if( kanter_get_option('al-blog-description') ):
                            echo '<p>'.esc_html( kanter_get_option('al-blog-description', 'Description blog') ).'</p>';
                        else:
                            bloginfo('description');
                        endif;
                        ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>


<?php get_template_part('loop/loop-post','default'); ?>

<?php get_template_part('templates/content/content', 'navigation'); //nav ?>



<?php get_footer(); ?>

<?php get_header(); ?>

    <section <?php echo kanter_blog_background(); ?> class="al-display-page al-bg-mask background-image">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="content">
                        <h1 class="al-heading-title-big"> <?php printf( esc_html__( 'Search Results for: %s', 'kanter' ), '<span>' . get_search_query() . '</span>' ); ?> </h1>
                    </div>
                </div>
            </div>
        </div>
    </section>


<?php get_template_part('loop/loop-post','default'); ?>

<?php get_template_part('templates/content/content', 'navigation'); //nav ?>

<?php get_footer(); ?>
<?php get_header(); ?>

    <section <?php echo kanter_blog_background(); ?> class="al-display-page al-bg-mask background-image">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="content">
                        <?php
                            the_archive_title( '<h1 class="al-heading-title-big">', '</h1>' );

                            $desc = get_the_archive_description();
                            if( $desc ){
                                echo wp_kses_post($desc);
                            }
                        ?>
                    </div>
                </div>
            </div>
        </div>
    </section>


<?php get_template_part('loop/loop-post','default'); ?>

<?php get_template_part('templates/content/content', 'navigation'); //nav ?>

<?php get_footer(); ?>
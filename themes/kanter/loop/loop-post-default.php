<div class="container single-blog">
    <div class="row">

        <!-- START Sidebar-->
        <div id="sidebar" class="col-md-3 col-sm-3 col-md-push-9 col-sm-push-9 right-sidebar">
            <?php get_sidebar(); ?>
        </div>
        <!-- END Sidebar-->

        <div class="col-md-8 col-sm-8 col-md-pull-3 col-sm-pull-3 single-post">

            <?php
                if(have_posts()):
            ?>

            <div class="">
                <?php
                    while (have_posts()) : the_post();

                        get_template_part('templates/content/content-post', get_post_format());

                    endwhile;
                ?>
            </div>

            <?php
                else:
                    get_template_part('templates/content/content', 'none'); //none
                endif;
            ?>

        </div>

    </div>
</div>
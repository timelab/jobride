<!DOCTYPE html>
<html <?php language_attributes(); ?> >
<head>

    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta charset="<?php bloginfo('charset'); ?>" />
    <meta name="viewport" content="width=device-width,initial-scale=1,maximum-scale=1,user-scalable=no">
    <meta name="format-detection" content="telephone=no">
    <link rel="profile" href="http://gmpg.org/xfn/11" />
    <link rel="pingback" href="<?php bloginfo('pingback_url'); ?>" />
    <?php wp_head(); ?>

</head>

<body id="top" <?php body_class(); ?>>

<?php

    $menu_class = 'sf-menu';
    $menu_id = '';

    if(kanter_menu_position() === 'left'){
        $menu_class .= ' sf-vertical';
    }else{
        $menu_id = 'id=top-nav';
    }
?>

<?php if(kanter_get_option('al-preloader') === 'on' ){
    echo '<div class="al-preloader-fixed" id="preloader"><div '.kanter_loader_style().' id="status"></div></div>';
} ?>

<?php if(kanter_menu_position() === 'left'){
    echo '<div class="content-wrap-lfm">';
} ?>

<!--Full screen top nav start-->
<?php if(kanter_get_option('al-fullscreen-nav-show') === 'on'){
    get_template_part('templates/header/header','fullscreen-nav');
} ?>
<!--Full screen top nav end-->

<header <?php echo kanter_nav_background(); ?> <?php echo esc_attr($menu_id); ?> class="<?php echo kanter_header_class(); ?>">
    <?php if(kanter_menu_position() === 'top'){
        echo '<div class="container">';
    }else{
        echo '<div class="al-nav-container-rel">';
    } ?>


        <?php get_template_part('templates/site/site','header-logo'); ?>

        <?php if(kanter_get_option('al-fullscreen-nav-show') === 'on' ){
            if(kanter_menu_position() === 'left'){
                echo '<a href="#" class="toggle-top al-toggle-full-nav"><span></span><span></span><span></span><span></span><span></span></a>';
            }
        } ?>

        <nav class="top-menu">
            <?php
                if(has_nav_menu('primary-menu'))
                {
                    wp_nav_menu(array(
                        'theme_location' => 'primary-menu',
                        'container' => FALSE,
                        'menu_class' => $menu_class,
                        'echo' => true
                    ));
                }
                else
                {
                    echo '<ul class="sf-menu">';
                    echo '<li><a class="selected" href="#">' . esc_html__('No Menu', 'kanter') . '</a></li>';
                    echo '</ul>';
                }
            ?>
            <!-- Start toggle menu-->
            <a href="#" class="toggle-mnu"><span></span></a>

            <!-- Start toggle menu-->
            <?php if(kanter_get_option('al-fullscreen-nav-show') === 'on' ){
                echo '<a href="#" class="toggle-top"><span></span><span></span><span></span><span></span><span></span></a>';
            } ?>


        </nav>


    <?php if(kanter_menu_position() === 'left'){
        if(kanter_get_option('al-left-menu-social') === 'true'){
            get_template_part('templates/site/site','social-icons');
        }
    } ?>


        <!-- Start mobile menu-->
        <div id="mobile-menu">
            <div class="inner-wrap">
                <nav>
                    <?php
                        if(has_nav_menu('mobile-menu'))
                        {
                            wp_nav_menu(array(
                                'theme_location' => 'mobile-menu',
                                'container' => FALSE,
                                'menu_class' => 'nav_menu',
                                'echo' => true
                            ));
                        }
                        else
                        {
                            echo '<ul class="nav_menu">';
                            echo '<li><a class="selected" href="#">' . esc_html__('No Menu', 'kanter') . '</a></li>';
                            echo '</ul>';
                        }
                    ?>
                </nav>
            </div>
        </div>
        <!-- End mobile menu-->

    </div>

</header>

<?php if(kanter_get_option('al-back-to-top') === 'on'){
    echo '<div class="top icon-down toTopFromBottom"><a href="#" class="al-btn-to-top"><i class="pe-7s-angle-up"></i></a></div>';
} ?>

<article class="<?php echo kanter_photos_folio_class(); ?>">
    <a href="<?php the_post_thumbnail_url() ?>" class="wrap-ms-pr link-portfolio dh-container">
        <div class="bg-mask-pr dh-overlay">
            <div class="icon"><i class="pe-7s-search"></i></div>
        </div>
        <img src="<?php the_post_thumbnail_url() ?>" class="img-responsive" alt="<?php echo get_the_title(); ?>" >
    </a>
</article>





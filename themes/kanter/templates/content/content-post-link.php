<article id="post-<?php the_ID(); ?>" <?php kanter_post_class(); ?> >

    <?php echo kanter_post_thunbnail_format(); ?>

    <div class="content">
        <?php echo kanter_post_title(); ?>

        <?php echo get_template_part('templates/content/content-post','meta'); ?>

            <?php the_content(); ?>


        <?php echo kanter_post_footer(); ?>
    </div>

</article>
<div class="al-post-data">
    <div class="al-post-author">
        <?php
            echo esc_html__('By ', 'kanter');
            echo '<a href="'.get_author_posts_url(get_the_author_meta( 'ID' )).'">';
                echo get_the_author_meta('nickname');
            echo ' </a> ';
        ?>
    </div>
    <?php
        echo esc_html__('in ', 'kanter');
        echo kanter_post_catogories();
        echo get_the_time( 'M d, Y' );
    ?>
</div>
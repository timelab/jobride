<article id="post-<?php the_ID(); ?>" <?php kanter_post_class(); ?> >

    <?php echo kanter_post_thunbnail_format(); ?>

    <?php

        echo kanter_post_title();

        get_template_part('templates/content/content-post', 'meta');

        wp_link_pages( array(
            'before' => '<div class="al-page-links"><span class="al-page-links-title">' . esc_html__( 'Pages:', 'kanter' ) . '</span>',
            'after' => '</div>',
            'link_before' => '<span>',
            'link_after' => '</span>',
        ) );

    ?>

    <div class="al-entry-content">
        <?php the_content(); ?>
    </div>

    <?php echo kanter_post_tags(); ?>

</article>
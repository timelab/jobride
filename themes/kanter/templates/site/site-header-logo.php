<?php

    $logo = '';
    $logo_white = '<img alt="" src="'.kanter_get_option('al-logo-white').'" class="logo-white">';
    $logo_dark = '<img alt="" src="'.kanter_get_option('al-logo-dark').'" class="logo-dark">';

    if(kanter_get_option('al-type-logo') === 'image'){
        $logo .= $logo_white.$logo_dark;
    }elseif(kanter_get_option('al-type-logo') === 'text'){
        $logo = kanter_get_option('al-text-logo');
    }
    if(empty(kanter_get_option('al-text-logo') || kanter_get_option('al-logo-white') || kanter_get_option('al-logo-dark'))){
     $logo = get_bloginfo('name');
    }

?>

<a href="<?php echo esc_url(home_url('/')); ?>" class="logo smooth-scroll">
    <?php echo wp_kses_post($logo); ?>
</a>



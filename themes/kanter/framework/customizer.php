<?php

$img_path = KANTER_THEME_DIRECTORY . 'assets/img/';

/***********************************************************
    Kirki config
***********************************************************/

add_filter('kirki/config', 'kanter_kirki_update_config');

if(!function_exists('kanter_kirki_update_config')) {
    function kanter_kirki_update_config($config) {
        $config['url_path'] = KANTER_THEME_DIRECTORY . 'framework/kirki';
        return $config;
    }
}

Kirki::add_config('kanter_customize', array(
    'option_type' => 'theme_mod',
    'capability' => 'edit_theme_options'
));

/***********************************************************
    Section General
***********************************************************/
require KANTER_REQUIRE_DIRECTORY . 'framework/customizer/section-general.php';

/***********************************************************
    Section Full Screen Nav
***********************************************************/
require_once KANTER_REQUIRE_DIRECTORY . 'framework/customizer/section-full-screen-nav.php';

/***********************************************************
    Section Blog
***********************************************************/
require_once KANTER_REQUIRE_DIRECTORY . 'framework/customizer/section-blog.php';

/***********************************************************
    Section Page
***********************************************************/
require_once KANTER_REQUIRE_DIRECTORY . 'framework/customizer/section-page.php';

/***********************************************************
    Section footer
***********************************************************/
require_once KANTER_REQUIRE_DIRECTORY . 'framework/customizer/section-footer.php';

/***********************************************************
    Section social
***********************************************************/
require_once KANTER_REQUIRE_DIRECTORY . 'framework/customizer/section-social.php';

/***********************************************************
    Section woocommerce
***********************************************************/
if ( in_array( 'woocommerce/woocommerce.php', apply_filters( 'active_plugins', get_option( 'active_plugins' ) ) ) ) {
    require_once KANTER_REQUIRE_DIRECTORY . 'framework/customizer/section-woocommerce.php';
}


/***********************************************************
    Section 404 page
***********************************************************/
require_once KANTER_REQUIRE_DIRECTORY . 'framework/customizer/section-404.php';



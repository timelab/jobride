<?php

/**
 *
 *
 * General settings
 *
 *
 */

Kirki::add_section( 'general_settings', array(
    'title'      => esc_attr__( 'General settings', 'kanter' ),
    'priority'   => 1,
    'capability' => 'edit_theme_options',
) );

Kirki::add_field( 'kanter', array(
    'type'        => 'radio-buttonset',
    'settings'    => 'al-fullscreen-nav-show',
    'label'       => esc_html__( 'Full screen top menu', 'kanter' ),
    'description'  => esc_html__( 'Turn On if you want display it', 'kanter' ),
    'section'     => 'general_settings',
    'default'     => 'off',
    'choices'     => array(
        'off' => esc_html__( 'Off', 'kanter' ),
        'on'   => esc_html__( 'On', 'kanter' )

    ),
) );



Kirki::add_field( 'kanter', array(
    'type'        => 'radio-buttonset',
    'settings'    => 'al-menu-position',
    'label'       => esc_html__( 'Menu position', 'kanter' ),
    'section'     => 'general_settings',
    'default'     => 'top',
    'choices'     => array(
        'top'   => esc_html__( 'Top', 'kanter' ),
        'left' => esc_html__( 'Left', 'kanter' ),
    ),
) );
Kirki::add_field( 'kanter', array(
    'type'        => 'radio-buttonset',
    'settings'    => 'al-left-menu-social',
    'label'       => esc_html__( 'Show social networks ?', 'kanter' ),
    'description'  => esc_html__( 'Showing social networks at the bottom menu', 'kanter' ),
    'section'     => 'general_settings',
    'default'     => 'true',
    'choices'     => array(
        'true'   => esc_html__( 'Yes', 'kanter' ),
        'false' => esc_html__( 'No', 'kanter' ),
    ),
    'active_callback'  => array(
        array(
            'setting'  => 'al-menu-position',
            'operator' => '==',
            'value'    => 'left',
        ),
    )
) );
Kirki::add_field( 'kanter', array(
    'type'        => 'radio-buttonset',
    'settings'    => 'al-relative-header',
    'label'       => esc_html__( 'Menu style', 'kanter' ),
    'section'     => 'general_settings',
    'default'     => 'true',
    'choices'     => array(
        'true'   => esc_html__( 'Absolute', 'kanter' ),
        'false' => esc_html__( 'Relative', 'kanter' ),
    ),
    'active_callback'  => array(
        array(
            'setting'  => 'al-menu-position',
            'operator' => '==',
            'value'    => 'top',
        ),
    )
) );

Kirki::add_field( 'kanter', array(
    'type'        => 'image',
    'settings'    => 'al-nav-background',
    'label'       => esc_html__( 'Menu background', 'kanter' ),
    'section'     => 'general_settings',
    'active_callback'  => array(
        array(
            'setting'  => 'al-menu-position',
            'operator' => '==',
            'value'    => 'left',
        ),
    )
) );

Kirki::add_field( 'kanter', array(
    'type'        => 'radio-buttonset',
    'settings'    => 'al-hide-scroll-menu',
    'label'       => esc_html__( 'Hide menu on scroll ?', 'kanter' ),
    'section'     => 'general_settings',
    'default'     => 'on',
    'choices'     => array(
        'on'   => esc_html__( 'On', 'kanter' ),
        'off' => esc_html__( 'Off', 'kanter' ),
    ),
) );

//Kirki::add_field( 'kanter', array(
//    'type'        => 'radio-image',
//    'settings'    => 'al-accent-color',
//    'label'       => esc_html__( 'Main color', 'kanter' ),
//    'description' => esc_attr__( 'Select the main color four your site..', 'kanter' ),
//    'help'        => esc_attr__( 'The color options you set here apply to all your site.', 'kanter' ),
//    'section'     => 'general_settings',
//    'default'     => '1',
//    'choices'     => array(
//        '1'   => get_template_directory_uri() . '/assets/img/customizer/1.png',
//        '2'   => get_template_directory_uri() . '/assets/img/customizer/2.png',
//        '3'   => get_template_directory_uri() . '/assets/img/customizer/3.png',
//        '4'   => get_template_directory_uri() . '/assets/img/customizer/4.png',
//        '5'   => get_template_directory_uri() . '/assets/img/customizer/5.png',
//        '6'   => get_template_directory_uri() . '/assets/img/customizer/6.png',
//        '7'   => get_template_directory_uri() . '/assets/img/customizer/7.png',
//        '8'   => get_template_directory_uri() . '/assets/img/customizer/8.png',
//        '9'   => get_template_directory_uri() . '/assets/img/customizer/9.png',
//        '10'   => get_template_directory_uri() . '/assets/img/customizer/10.png',
//        '11'   => get_template_directory_uri() . '/assets/img/customizer/11.png',
//        '12'   => get_template_directory_uri() . '/assets/img/customizer/12.png',
//
//    ),
//) );

//Kirki::add_field( 'kanter', array(
//    'type'        => 'typography',
//    'settings'    => 'al-nav-typography',
//    'label'       => esc_attr__( 'Typography for navigation', 'kanter' ),
//    'description' => esc_attr__( 'Select the main typography options for main menu.', 'kanter' ),
//    'help'        => esc_attr__( 'The typography options you set here apply to your menu.', 'kanter' ),
//    'section'     => 'general_settings',
//    'default'     => array(
//        'font-family'    => 'Roboto Slab',
//        'variant'        => '700',
//        'font-size'      => '16px',
//        'letter-spacing' => '0.3px',
//        'line-height'    => '24px',
//    ),
//    'output' => array(
//        array(
//            'element' => '.sf-menu > li',
//        ),
//    ),
//) );

Kirki::add_field( 'kanter', array(
    'type'        => 'radio-buttonset',
    'settings'    => 'al-type-logo',
    'label'       => esc_html__( 'Select type of logo', 'kanter' ),
    'section'     => 'general_settings',
    'default'     => 'text',
    'choices'     => array(
        'text'   => esc_html__( 'Text', 'kanter' ),
        'image' => esc_html__( 'Image', 'kanter' ),
    ),
) );

Kirki::add_field( 'kanter', array(
    'type'        => 'image',
    'settings'    => 'al-logo-white',
    'label'       => esc_html__( 'White logo', 'kanter' ),
    'section'     => 'general_settings',
    'active_callback'  => array(
        array(
            'setting'  => 'al-type-logo',
            'operator' => '==',
            'value'    => 'image',
        ),
    )
) );

Kirki::add_field( 'kanter', array(
    'type'        => 'image',
    'settings'    => 'al-logo-dark',
    'label'       => esc_html__( 'Dark logo', 'kanter' ),
    'section'     => 'general_settings',
    'active_callback'  => array(
        array(
            'setting'  => 'al-type-logo',
            'operator' => '==',
            'value'    => 'image',
        ),
    )
) );

Kirki::add_field( 'kanter', array(
    'type'     => 'text',
    'settings' => 'al-text-logo',
    'label'    => esc_html__( 'Insert your logo', 'kanter' ),
    'section'  => 'general_settings',
    'default'     => '',
    'sanitize_callback' => 'wp_kses_post',
    'active_callback'  => array(
        array(
            'setting'  => 'al-type-logo',
            'operator' => '==',
            'value'    => 'text',
        ),
    )
) );

Kirki::add_field( 'kanter', array(
    'type'        => 'typography',
    'settings'    => 'al-typography-logo',
    'label'       => esc_attr__( 'Typography for logo', 'kanter' ),
    'description' => esc_attr__( 'Select the main typography options for your logo.', 'kanter' ),
    'help'        => esc_attr__( 'The typography options you set here apply to your logo.', 'kanter' ),
    'section'     => 'general_settings',
    'default'     => array(
        'font-family'    => 'Pacifico',
        'variant'        => '500',
        'font-size'      => '29px',
        'letter-spacing' => '0',
        'color'          => '#f6f5f5',
    ),
    'output' => array(
        array(
            'element' => '.logo',
        ),
    ),
    'active_callback'  => array(
        array(
            'setting'  => 'al-type-logo',
            'operator' => '==',
            'value'    => 'text',
        ),
    )
) );

Kirki::add_field( 'kanter', array(
    'type'        => 'radio-buttonset',
    'settings'    => 'al-preloader',
    'label'       => esc_html__( 'Preloader', 'kanter' ),
    'description'  => esc_html__( 'Turn On if you want display it', 'kanter' ),
    'section'     => 'general_settings',
    'default'     => 'on',
    'choices'     => array(
        'on'   => esc_html__( 'On', 'kanter' ),
        'off' => esc_html__( 'Off', 'kanter' ),
    ),
) );

Kirki::add_field( 'kanter', array(
    'type'        => 'color',
    'settings'    => 'al-preloader-color',
    'label'       => esc_attr__( 'Background', 'kanter' ),
    'description' => esc_attr__( 'Preloader background color.', 'kanter' ),
    'section'     => 'general_settings',
    'default'     => '#555',
    'active_callback'  => array(
        array(
            'setting'  => 'al-preloader',
            'operator' => '==',
            'value'    => 'on',
        ),
    )
) );

Kirki::add_field( 'kanter', array(
    'type'        => 'radio-buttonset',
    'settings'    => 'al-back-to-top',
    'label'       => esc_html__( 'Back to top', 'kanter' ),
    'description'  => esc_html__( 'Turn On if you want display it', 'kanter' ),
    'section'     => 'general_settings',
    'default'     => 'on',
    'choices'     => array(
        'on'   => esc_html__( 'On', 'kanter' ),
        'off' => esc_html__( 'Off', 'kanter' ),
    ),
) );


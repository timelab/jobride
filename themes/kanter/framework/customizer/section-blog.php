<?php

/**
 *
 *
 * Blog settings
 *
 *
 */

Kirki::add_section( 'blog_settings', array(
    'title'      => esc_attr__( 'Blog settings', 'kanter' ),
    'priority'   => 3,
    'capability' => 'edit_theme_options',
) );

Kirki::add_field( 'kanter', array(
    'type'     => 'text',
    'settings' => 'al-blog-heading',
    'label'    => esc_html__( 'Heading title', 'kanter' ),
    'section'  => 'blog_settings',
) );

Kirki::add_field( 'kanter', array(
    'type'     => 'textarea',
    'settings' => 'al-blog-description',
    'label'    => esc_html__( 'Description', 'kanter' ),
    'section'  => 'blog_settings',
) );

Kirki::add_field( 'kanter', array(
    'type'        => 'image',
    'settings'    => 'al-blog-background',
    'label'       => esc_html__( 'Background image', 'kanter' ),
    'section'     => 'blog_settings',
) );

Kirki::add_field( 'kanter', array(
    'type'        => 'image',
    'settings'    => 'al-archive-background',
    'label'       => esc_html__( 'Background image: Archive page', 'kanter' ),
    'section'     => 'blog_settings',
) );

Kirki::add_field( 'kanter', array(
    'type'        => 'image',
    'settings'    => 'al-search-background',
    'label'       => esc_html__( 'Background image: Search page', 'kanter' ),
    'section'     => 'blog_settings',
) );

Kirki::add_field( 'kanter', array(
    'type'        => 'radio-buttonset',
    'settings'    => 'al-show-share',
    'label'       => esc_html__( 'Showing share buttons ?', 'kanter' ),
    'description'  => esc_html__( 'Turn On if you want display it', 'kanter' ),
    'section'     => 'blog_settings',
    'default'     => 'on',
    'choices'     => array(
        'on'   => esc_html__( 'On', 'kanter' ),
        'off' => esc_html__( 'Off', 'kanter' ),
    ),
) );
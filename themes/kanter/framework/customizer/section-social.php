<?php

Kirki::add_section( 'social_settings', array(
    'title'      => esc_attr__( 'Social settings', 'kanter' ),
    'capability' => 'edit_theme_options',
) );

Kirki::add_field( 'kanter', array(
    'type'        => 'select',
    'settings'    => 'al-social-target',
    'label'       => esc_html__( 'Target:', 'kanter' ),
    'default'     => '_self',
    'section'     => 'social_settings',
    'multiple'    => 1,
    'choices'     => array(
        '_self' => esc_attr__( 'This site', 'kanter' ),
        '_blank' => esc_attr__( 'New page', 'kanter' )
    ),
) );

Kirki::add_field( 'kanter', array(
    'type'     => 'text',
    'settings' => 'al-facebook',
    'label'    => esc_html__( 'Facebook', 'kanter' ),
    'description'  => esc_html__( 'Insert your link here', 'kanter' ),
    'section'  => 'social_settings',
    'default'     => '#',
) );

Kirki::add_field( 'kanter', array(
    'type'     => 'text',
    'settings' => 'al-twitter',
    'label'    => esc_html__( 'Twitter', 'kanter' ),
    'description'  => esc_html__( 'Insert your link here', 'kanter' ),
    'section'  => 'social_settings',
    'default'     => '#',
) );

Kirki::add_field( 'kanter', array(
    'type'     => 'text',
    'settings' => 'al-linkedin',
    'label'    => esc_html__( 'Linkedin', 'kanter' ),
    'description'  => esc_html__( 'Insert your link here', 'kanter' ),
    'section'  => 'social_settings',
    'default'     => '#',
) );

Kirki::add_field( 'kanter', array(
    'type'     => 'text',
    'settings' => 'al-instagram',
    'label'    => esc_html__( 'Instagram', 'kanter' ),
    'description'  => esc_html__( 'Insert your link here', 'kanter' ),
    'section'  => 'social_settings',
    'default'     => '#',
) );

Kirki::add_field( 'kanter', array(
    'type'     => 'text',
    'settings' => 'al-google',
    'label'    => esc_html__( 'Google Plus', 'kanter' ),
    'description'  => esc_html__( 'Insert your link here', 'kanter' ),
    'section'  => 'social_settings',
) );

Kirki::add_field( 'kanter', array(
    'type'     => 'text',
    'settings' => 'al-skype',
    'label'    => esc_html__( 'Skype', 'kanter' ),
    'description'  => esc_html__( 'Insert your profile nick', 'kanter' ),
    'section'  => 'social_settings',
) );

Kirki::add_field( 'kanter', array(
    'type'     => 'text',
    'settings' => 'al-dribbble',
    'label'    => esc_html__( 'Dribbble', 'kanter' ),
    'description'  => esc_html__( 'Insert your link here', 'kanter' ),
    'section'  => 'social_settings',
) );

Kirki::add_field( 'kanter', array(
    'type'     => 'text',
    'settings' => 'al-pinterest',
    'label'    => esc_html__( 'Pinterest', 'kanter' ),
    'description'  => esc_html__( 'Insert your link here', 'kanter' ),
    'section'  => 'social_settings',
) );

Kirki::add_field( 'kanter', array(
    'type'     => 'text',
    'settings' => 'al-rss',
    'label'    => esc_html__( 'RSS', 'kanter' ),
    'description'  => esc_html__( 'Insert your link here', 'kanter' ),
    'section'  => 'social_settings',
) );

Kirki::add_field( 'kanter', array(
    'type'     => 'text',
    'settings' => 'al-youtube',
    'label'    => esc_html__( 'Youtube', 'kanter' ),
    'description'  => esc_html__( 'Insert your link here', 'kanter' ),
    'section'  => 'social_settings',
) );

Kirki::add_field( 'kanter', array(
    'type'     => 'text',
    'settings' => 'al-vimeo',
    'label'    => esc_html__( 'Vimeo', 'kanter' ),
    'description'  => esc_html__( 'Insert your link here', 'kanter' ),
    'section'  => 'social_settings',
) );

Kirki::add_field( 'kanter', array(
    'type'     => 'text',
    'settings' => 'al-bitbucket',
    'label'    => esc_html__( 'Bitbucket', 'kanter' ),
    'description'  => esc_html__( 'Insert your link here', 'kanter' ),
    'section'  => 'social_settings',
) );

Kirki::add_field( 'kanter', array(
    'type'     => 'text',
    'settings' => 'al-github',
    'label'    => esc_html__( 'Github', 'kanter' ),
    'description'  => esc_html__( 'Insert your link here', 'kanter' ),
    'section'  => 'social_settings',
) );
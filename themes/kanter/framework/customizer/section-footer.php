<?php

/**
 *
 *
 * Footer settings
 *
 *
 */

Kirki::add_section( 'footer_settings', array(
    'title'      => esc_attr__( 'Footer settings', 'kanter' ),
    'priority'   => 4,
    'capability' => 'edit_theme_options',
) );

Kirki::add_field( 'kanter', array(
    'type'     => 'textarea',
    'settings' => 'al-footer-copyright',
    'default' => esc_html__('2018 All Rights Reserved.  -   Created by Brainiakthemes', 'kanter'),
    'label'    => esc_html__( 'Copyright', 'kanter' ),
    'description'  => esc_html__( 'Insert your copyright here', 'kanter' ),
    'section'  => 'footer_settings',
) );

Kirki::add_field( 'kanter', array(
    'type'     => 'textarea',
    'settings' => 'al-footer-description',
    'label'    => esc_html__( 'Description', 'kanter' ),
    'description'  => esc_html__( 'Insert your description here', 'kanter' ),
    'section'  => 'footer_settings',
    'active_callback'  => array(
        array(
            'setting'  => 'al-type-footer',
            'operator' => '==',
            'value'    => 'minimal',
        ),
    )
) );


Kirki::add_field( 'kanter', array(
    'type'        => 'select',
    'settings'    => 'al-type-footer',
    'label'       => esc_html__( 'Choose the type of footer', 'kanter' ),
    'section'     => 'footer_settings',
    'default'     => 'default',
    'multiple'    => 1,
    'choices'     => array(
        'default' => esc_attr__( 'Default', 'kanter' ),
        'minimal' => esc_attr__( 'Minimal', 'kanter' ),
        'hide' => esc_attr__( 'Hide', 'kanter' )
    )
) );
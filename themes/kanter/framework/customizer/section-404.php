<?php

/**
 *
 *
 * 404 settings
 *
 *
 */

Kirki::add_section( '404_settings', array(
    'title'      => esc_attr__( '404 settings', 'kanter' ),
    'priority'   => 6,
    'capability' => 'edit_theme_options',
) );

Kirki::add_field( 'kanter', array(
    'type'     => 'text',
    'settings' => 'al-404-title',
    'label'    => esc_html__( 'Heading title', 'kanter' ),
    'section'  => '404_settings',
    'default'  => esc_attr__( 'Page 404', 'kanter' ),

) );

Kirki::add_field( 'kanter', array(
    'type'     => 'textarea',
    'settings' => 'al-404-description',
    'label'    => esc_html__( 'Description', 'kanter' ),
    'section'  => '404_settings',
    'default'  => esc_attr__( 'Oops, Page Not Found', 'kanter' ),

) );

Kirki::add_field( 'kanter', array(
    'type'     => 'text',
    'settings' => 'al-404-label',
    'label'    => esc_html__( 'Button label', 'kanter' ),
    'section'  => '404_settings',
    'default'  => esc_attr__( 'Go to back', 'kanter' ),

) );

Kirki::add_field( 'kanter', array(
    'type'        => 'image',
    'settings'    => 'al-404-image',
    'label'       => esc_html__( 'Background image', 'kanter' ),
    'section'     => '404_settings',
) );


<?php

/**
 *
 *
 * Woocommerce settings
 *
 *
 */

Kirki::add_section( 'woocommerce', array(
    'title'      => esc_attr__( 'Shop settings', 'kanter' ),
    'capability' => 'edit_theme_options',
) );

Kirki::add_field( 'kanter', array(
    'type'        => 'image',
    'settings'    => 'al-background-woocommerce-page',
    'label'       => esc_html__( 'Background page image', 'kanter' ),
    'section'     => 'woocommerce',
) );

Kirki::add_field( 'kanter', array(
    'type'     => 'text',
    'settings' => 'al-heading-archive-woocommerce',
    'label'    => esc_html__( 'Heading title', 'kanter' ),
    'section'  => 'woocommerce',
    'default'  => esc_attr__( 'Shop', 'kanter' ),
) );

Kirki::add_field( 'kanter', array(
    'type'     => 'textarea',
    'settings' => 'al-description-archive-woocommerce',
    'label'    => esc_html__( 'Description', 'kanter' ),
    'section'  => 'woocommerce',
    'default'  => esc_attr__( 'Description', 'kanter' ),
) );


Kirki::add_field( 'kanter', array(
    'type'        => 'switch',
    'settings'    => 'al-sidebar-archive',
    'label'       => esc_html__( 'Show sidebar on Arhive page ?', 'kanter' ),
    'description'  => esc_html__( 'Turn On if you want display it', 'kanter' ),
    'section'     => 'woocommerce',
    'default'     => '1',
    'choices'     => array(
        'on'  => esc_attr__( 'Enable', 'kanter' ),
        'off' => esc_attr__( 'Disable', 'kanter' ),
    ),
) );


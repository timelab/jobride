<?php

/****************************
Required Plugins
 ***************************/

add_action( 'tgmpa_register', 'kanter_register_required_plugins' );

if(! function_exists('kanter_register_required_plugins')) {

    function kanter_register_required_plugins() {

        $plugins = array(
            array(
                'name'               => 'Kanter Helper', // The plugin name.
                'slug'               => 'kanter-helper', // The plugin slug (typically the folder name).
                'source'             => get_template_directory() . '/assets/plugins/kanter-helper.zip', // The plugin source.
                'required'           => true, // If false, the plugin is only 'recommended' instead of required.
            ),
            array(
                'name'               => 'Advanced Custom Fields Pro', // The plugin name.
                'slug'               => 'advanced-custom-fields-pro', // The plugin slug (typically the folder name).
                'source'             => esc_url('http://alian4x.com/plugins/advanced-custom-fields-pro.zip'),
                'required'           => true, // If false, the plugin is only 'recommended' instead of required.
            ), 
            array(
                'name'               => 'Slider Revolution', // The plugin name.
                'slug'               => 'rev-slider', // The plugin slug (typically the folder name).
                'source'             => esc_url('http://alian4x.com/plugins/slider-revolution.zip'),
            ),
            array(
                'name'               => 'Visual Composer', // The plugin name.
                'slug'               => 'visual-composer', // The plugin slug (typically the folder name).
                'source'             => esc_url('http://alian4x.com/plugins/js_composer.zip'),
            ),
            array(
                'name'               => 'Visual portfolio', // The plugin name.
                'slug'               => 'visual-portfolio', // The plugin slug (typically the folder name).
            ),
            array(
                'name'               => 'Ultimate VC Addons', // The plugin name.
                'slug'               => 'ultimate-composer', // The plugin slug (typically the folder name).
                'source'             => esc_url('http://alian4x.com/plugins/Ultimate_VC_Addons.zip'),
            ),
            array(
                'name'      => esc_html__( 'One click Demo Import', 'kanter' ),
                'slug'      => 'one-click-demo-import',
            ),
            array(
                'name'      => esc_html__( 'Contact form 7', 'kanter' ),
                'slug'      => 'contact-form-7',
            ),
            array(
                'name'      => esc_html__( 'Woocommerce', 'kanter' ),
                'slug'      => 'woocommerce',
            )

            );
        tgmpa( $plugins);
    }
}

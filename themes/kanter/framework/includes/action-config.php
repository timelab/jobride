<?php

/****************************
    Register Fonts
***************************/
function kanter_fonts_url() {
    $font_url = '';

    if ( 'off' !== _x( 'on', 'Google font: on or off', 'kanter' ) ) {
        $font_url = add_query_arg( 'family', urlencode( 'Roboto Slab:400,500,600,700,300|Roboto:400,500,700,300|Pacifico|Poppins|Lato:900' ), "//fonts.googleapis.com/css" );
    }
    return $font_url;
}

/****************************
    Enqueue Scripts
***************************/
add_action('wp_enqueue_scripts', 'kanter_enqueue_scripts');

if (!function_exists('kanter_enqueue_scripts')) {
    function kanter_enqueue_scripts() {

        $info_theme = wp_get_theme();

        if(is_singular() && comments_open()) {
            wp_enqueue_script('comment-reply');
        }

        wp_enqueue_script('kanter_plugins', KANTER_THEME_DIRECTORY . 'assets/scripts/libs.js', array('jquery'), $info_theme->get('Version'), true );
        wp_enqueue_script('kanter_common', KANTER_THEME_DIRECTORY . 'assets/scripts/common.js', array('jquery'), $info_theme->get('Version'), true );

    }
}

/****************************
    Enqueue Style
***************************/
add_action('wp_enqueue_scripts', 'kanter_enqueue_styles');

if (!function_exists('kanter_enqueue_styles')) {
    function kanter_enqueue_styles() {

        $info_theme = wp_get_theme();

        wp_enqueue_style( 'kanter-fonts', kanter_fonts_url(), array(), '1.0.0' );

        wp_enqueue_style('kanter_plugins', KANTER_THEME_DIRECTORY . 'assets/css/libs.css', array(), $info_theme->get('Version'));
        wp_enqueue_style('kanter_main', KANTER_THEME_DIRECTORY . 'assets/css/main.css', array(), $info_theme->get('Version'));

    }
}
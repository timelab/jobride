<?php

/****************************
    Widgets
***************************/
add_action( 'widgets_init', 'kanter_widget_init' );
if(!function_exists('kanter_widget_init')) {

    function kanter_widget_init() {

        register_sidebar( array(
            'name'          => esc_html__( 'Blog Sidebar', 'kanter' ),
            'id'            => 'blog_sidebar',
            'description'   => esc_html__( 'Add widgets here.', 'kanter' ),
            'before_widget' => '',
            'after_widget'  => '',
            'before_title'  => '<h4>',
            'after_title'   => '</h4>',
        ) );
        register_sidebar( array(
            'name'          => esc_html__( 'Footer Sidebar', 'kanter' ),
            'id'            => 'footer_sidebar',
            'description'   => esc_html__( 'Add widgets here.', 'kanter' ),
            'before_widget' => '<div class="col-md-3"><div id="%1$s" class="al-widget %2$s">',
            'after_widget'  => '</div></div>',
            'before_title'  => '<h5 class="al-widget-title">',
            'after_title'   => '</h5>',
        ) );

    }
}

/****************************
    Search form
***************************/
add_filter( 'get_search_form', 'kanter_search_form', 100 );
if (!function_exists('kanter_search_form')) {

    function kanter_search_form( $kanter_form ) {

        $kanter_form = '<form role="search" method="get" id="searchform" class="searchform" action="' . home_url( '/' ) . '" >
        <div class="search-form">
        <input type="text" placeholder="'. esc_attr__( 'Search', 'kanter' ) .'" value="' . get_search_query() . '" name="s" id="s" />
        <button class="search-submit"><i class="fa fa-search"></i></button>
        </div>
        </form>';

        return $kanter_form;
    }
}

/****************************
    Left nav - adding class
***************************/
add_filter( 'body_class', 'kanter_body_class' );
if(!function_exists('kanter_body_class')) {
    function kanter_body_class( $classes ) {

        if(class_exists('acf')){
            $menu_style = get_field('page_style_menu');
        }
        if ( kanter_menu_position() === 'left') {
            $classes[] = 'al-sidenav';
        }
        if(kanter_get_option('al-relative-header') == 'false' || $menu_style == 'relative'){
            $classes[] = 'al-relative-header';
        }

        return $classes;
    }
}

/****************************
    Post footer
***************************/
if(!function_exists('kanter_post_footer')) {
    function kanter_post_footer() {
        global $post;

        $output = '';

        $output .= '<a href="'.get_permalink($post->ID).'" title="'.get_the_title($post->ID).'" class="btn btn-default">'.esc_html__('Read more','kanter').'</a><span class="format"></span>';

        return $output;
    }
}

/****************************
    Portfolio footer
***************************/
if(!function_exists('kanter_portfolio_footer')) {
    function kanter_portfolio_footer() {
        global $post;
        if(class_exists('acf')){
            $display_btn_see = get_field('portfolio_display_button_see');
            $button_label = get_field('portfolio_see_project_label');
        }

        $output = '';
        if($display_btn_see === true){
            $output .= '<hr><div class="buttons-section center"><a href="'.get_permalink($post->ID).'" title="'.get_the_title($post->ID).'" class="btn accent-br-btn large-btn">'.$button_label.'</a></div>';
        }



        return $output;
    }
}

/****************************
    Header class
***************************/
if(!function_exists('kanter_header_class')) {
    function kanter_header_class() {

        $output = 'top-nav';

        if(class_exists('acf')){
            $menu_style = get_field('page_style_menu');
        }

        if(kanter_get_option('al-relative-header') == 'false' || $menu_style == 'relative'){
            $output .= ' al-relative-header';
        }

        if(kanter_get_option('al-hide-scroll-menu') === 'on'){
            $output .= ' page-header';
        }


        if(kanter_menu_position() === 'left'){
            $output = 'top-nav header-type-1 background-image';
        }
        if(kanter_get_option('al-fullscreen-nav-show') === 'on'){
            $output .= ' al-have-full-screen';
        }


        return $output;
    }
}

/****************************
    Portfolio slug class
***************************/
if(!function_exists('kanter_folio_class')){
    function kanter_folio_class(){
        $folio_class = '';
        $output = '';

        if(class_exists('acf')){
            $folio_type = get_field('portfolio_type');
        }

        $output .= $folio_class ;

        $categories = get_the_terms(get_the_ID(), 'folio_cat');
        if (is_array($categories)) {
            foreach ($categories as $cats) {
                $output .= ' folio-'. $cats->slug .'';
            }
        }

        if($folio_type === 'photo_gallery' || $folio_type === 'audio_gallery' || $folio_type === 'video_gallery'){
            $output .= ' al-gallery-folio';
        }

        return $output;
    }
}

/****************************
    Portfolio slug class
***************************/
if(!function_exists('kanter_photos_folio_class')){
    function kanter_photos_folio_class(){
        $folio_class = '';
        $output = '';

        $output .= $folio_class ;

        $categories = get_the_terms(get_the_ID(), 'folio_photos_cat');
        if (is_array($categories)) {
            foreach ($categories as $cats) {
                $output .= ' folio-'. $cats->slug .'';
            }
        }

        return $output;
    }
}

/****************************
    Post class
***************************/
if(!function_exists('kanter_post_class')) {
    function kanter_post_class( $output="" ) {
        global $post;

        $class_posts = 'post type-post';
        $class_posts .= ' ' . $output;

        if(!is_single()){
            if(is_sticky($post->ID)){
                $class_posts .= ' ' . 'sticky';
            }
        }


        $post_class = post_class( $class_posts );

        return $post_class;
    }
}

/****************************
    Post navigation
***************************/
if(!function_exists('kanter_post_navigation')) {
    function kanter_post_navigation($query = null) {
        if($query == null){
            global $wp_query;
            $query = $wp_query;
        }

        $page = $query->query_vars['paged'];
        $pages = $query->max_num_pages;
        $paged = get_query_var('paged') ? get_query_var('paged') : (get_query_var('page') ? get_query_var('page') : 1);

        if($page == 0) {
            $page = 1;
        }

        $numeric_links = paginate_links(array(
            'foramt' => '',
            'add_args' => '',
            'current' => $paged,
            'total' => $pages,
            'prev_text' => esc_html__('Prev', 'kanter'),
            'next_text' => esc_html__('Next', 'kanter'),
        ));

        $output = '';

        if($pages > 1) {

                $output .= '<nav class="al-blog-pagination">';
                $output .= $numeric_links;
                $output .= '</nav>';

        return $output;
        }
    }
}

/****************************
    Excerpt_more
***************************/
add_filter( 'excerpt_more', 'kanter_excerpt_more' );

if(!function_exists('kanter_excerpt_more')) {
    function kanter_excerpt_more( $more ) {
        global $post;

        return '<span class="screen-reader-text"><a href="'.get_permalink($post->ID).'" rel="nofollow">'.esc_html__('[...]','kanter').'</a></span>';
    }
}

/****************************
    Post Title
***************************/
if(!function_exists('kanter_post_title')) {
    function kanter_post_title() {
        if ( is_single() ) {
            the_title( '<h1 class="al-entry-title">', '</h1>' );
        } else {
            the_title( '<h3 class="al-entry-title"><a href="' . esc_url( get_permalink() ) . '" rel="bookmark">', '</a></h3>' );
        }
    }
}

/****************************
    Navigation Background
***************************/
if(!function_exists('kanter_nav_background')){
    function kanter_nav_background() {

        $output = '';

        if(kanter_menu_position() === 'left'){
            $output = 'data-image="'.kanter_get_option('al-nav-background').'"';
        }

        return $output;
    }
}

/****************************
    Page Background
***************************/
if(!function_exists('kanter_page_background')){
    function kanter_page_background() {

        global $woocommerce;

        //START Woocommerce get info category
        global $wp_query;
        $cat = $wp_query->get_queried_object();

        $thumbnail_id = "";
        if(function_exists('is_product_category')){
            if( is_product_category() ) {
                $thumbnail_id = get_woocommerce_term_meta( $cat->term_id, 'thumbnail_id', true );
            }
        }

        $image = wp_get_attachment_url( $thumbnail_id );
        //END Woocommerce get info category
        $output = '';

        $output .= 'data-image="';

        if(is_page()){
            if( has_post_thumbnail()  ){
                $output .= kanter_get_thumbnail(null, 'full');
            }else{
                $output .= kanter_get_option('al-bg-page');
            }
        }
        if(is_404() && kanter_get_option('al-404-image')){
            $output .= kanter_get_option('al-404-image');
        }
        if( $woocommerce && is_shop() || $woocommerce && is_product_tag()){
            $output .= kanter_get_option('al-background-woocommerce-page');
        }elseif($woocommerce && is_product_category() ){
            if($image){
                $output .= $image;
            }else{
                $output .= kanter_get_option('al-background-woocommerce-page');
            }
        }

        $output .= '"';

        return $output;
    }

}

/****************************
    Page title&description
***************************/
if(!function_exists('kanter_page_description')){
    function kanter_page_description(){

        global $woocommerce;
        global $wp_query;
        $cat = $wp_query->get_queried_object();

        $output = '';

        if($woocommerce && is_shop() ){
            $output .= '<h1 class="al-heading-title-big">'.kanter_get_option('al-heading-archive-woocommerce', 'shop').'</h1>';
            $output .= '<p>'.kanter_get_option('al-description-archive-woocommerce', 'Description').'</p>';
        }elseif( $woocommerce && is_product_category()){
            $output .= '<h1 class="al-heading-title-big"><span>'.esc_html__('Category:', 'kanter').' </span>'.$cat->name.'</h1>';
            if($cat->description !== ''){
                $output .= '<p>'.$cat->description .'</p>';
            }
        }else{
            $output .= the_title( '<h1 class="al-heading-title-big">', '</h1>' );
        }

        return $output;

    }
}

/****************************
    Background page blog
***************************/
if(!function_exists('kanter_blog_background')){

    function kanter_blog_background(){

        $output = '';

        $bg_image = kanter_get_option('al-blog-background');

        if(class_exists('acf')){
            $blog_background_image = get_field('blog_background_image');
        }

        $output = 'data-image="';

        if( is_category() || is_home() || is_archive() ){
            $output .= kanter_get_option('al-archive-background');
        }
        if( is_search() ){
            $output .= kanter_get_option('al-blog-background');
        }
        if( is_page_template('blog.php') ){
            if(!empty($blog_background_image)){
                $output .= $blog_background_image;
            }else{
                $output .= kanter_get_option('al-blog-background');
            }
        }

        $output .= '"';

        return $output;
    }
}

/****************************
   Get Post Thumbnail
***************************/
if(!function_exists('kanter_get_thumbnail')){
    function kanter_get_thumbnail($post_id, $size) {
        $thumb = wp_get_attachment_image_src(get_post_thumbnail_id($post_id), $size);
        return $thumb['0'];
    }
}

/****************************
    Post Tags
***************************/
if(!function_exists('kanter_post_tags')){
    function kanter_post_tags(){
        $output = '';

        if(get_the_tags()){

            $tags = get_the_tags();
            $separator = "";
            $out_tags = '';

            foreach ( $tags as $tag ) {
                $tag_link = get_tag_link( $tag->term_id );

                $out_tags .= "<a href='{$tag_link}' title='{$tag->name} Tag'> {$tag->name}</a>";

                $done_tags = trim($out_tags, $separator);
            }

            $output .='<div class="al-post-tags"><span>'.esc_html__('Tags:','kanter').'</span> '.$done_tags.'</div>';
        }

        return $output;
    }
}

/****************************
    Post Categories
***************************/
if(!function_exists('kanter_post_categories')){
    function kanter_post_catogories(){
        global $post;
        $categories = wp_get_object_terms($post->ID, 'category');

        $output = '';

        if (is_array($categories)) {

            $s = 1;
            foreach ($categories as $cats) {
                $output .= '<div class="al-category"><a href="' . get_category_link($cats->term_id) . '">' . $cats->name . '</a></div>';
                $s++;
            }
        } else {
            $output .='<div class="al-category" ><a href="#">'.esc_html__('Uncategorized', 'kanter').'</a></div>';
        }

        return $output;
    }
}

/****************************
    Post Thumbnails Format
***************************/
if(!function_exists('kanter_post_thumbnail_format')){
    function kanter_post_thunbnail_format($format='', $size='kanter_horizont_post') {

        global $post;
        $format = get_post_format( $post->ID );
        global $wp_embed;


        $output = '';

        if($format == 'gallery'){
            if(class_exists('acf')) {

                $images = get_field('post_gallery_images');

                $output .= '<div class="post-thumb">';
                $output .= '';
                $output .= '<div class="slider-wrap gallery-slide">';
                $output .= '<div class="slider-portfolio-single">';
                foreach ($images as $image) {
                    $output .= '<img src="' . $image['sizes'][$size] . '" alt="' . $image['alt'] . '">';
                }
                $output .= '</div>';
                $output .= '<div class="prev-next-block-rotate opacity-control al-control-portfolio-slider">
                                <div class="wrap-prev">
                                  <div class="prev"><i aria-hidden="true" class="fa fa-angle-left"></i></div>
                                </div>
                                <div class="wrap-next">
                                  <div class="next"><i aria-hidden="true" class="fa fa-angle-right"></i></div>
                                </div>
                              </div>
                            <!--Control sliders-->
                            <div class="al-dots-control dots-control-carousel"></div>';
                $output .= '</div>';
                $output .= '</div>';
            }
        }elseif($format == 'audio'){
            if(class_exists('acf')):
                $type = get_field('post_audio_type');
                $output .= '<div class="post-thumb">';
                $output .= '';
                switch ($type){
                    case 'link';
                        $audio = get_field('post_local_media');
                        $output .= do_shortcode('[audio src="'.$audio.'" ]');
                        break;
                    case 'iframe';
                        $audio = get_field('post_iframe_audio');
                        $output .= $audio;
                        break;
                }
                $output .= '</div>';
            endif;

        }elseif($format == 'video'){
            $output .= '<div class="post-thumb">';
                $video = $wp_embed->run_shortcode( ' [embed]' . trim( get_field('post_video_iframe') ) . '[/embed]' );

                $output .= '<div class="embed-responsive">';
                    $output .= $video;
                $output .= '</div>';
            $output .= '</div>';
                }else{
                    if(has_post_thumbnail()):
                        if(!is_single()){ $output .='<a href="'.get_the_permalink($post->ID).'">'; }
                        $output .= '<div class="post-thumb">';
                            $output .='<img src="'.kanter_get_thumbnail($post->ID, $size).'" alt="'.get_the_title().'" >';
                        $output .= '</div>';
                        if(!is_single()){ $output .='</a>'; }
                    endif;

                }

        return $output;

    }
}

/****************************
    Comments layout
***************************/
if (!function_exists('kanter_comments')) {

    function kanter_comments($comment, $args, $depth) {
        $GLOBAL['comment'] = $comment;
        ?>
        <li class="al-comment-item">
            <div id="comment-<?php comment_ID(); ?>" <?php comment_class(); ?>>

                <div class="al-comment-photo">
                    <?php echo get_avatar($comment, 90, $default='') ?>
                </div>

                <div class="al-comment-content">

                    <div class="al-comment-header">
                        <span class="al-comment-author"><?php comment_author(); ?></span>
                        <a href="<?php echo esc_url(get_comment_link($comment->comment_ID)) ?>" class="al-comment-date"><?php printf( ' %1$s ' . esc_html__('at', 'kanter') . ' %2$s', get_comment_date(),  get_comment_time() ); ?></a>
                    </div>

                    <div class="al-comment-text">

                        <?php if ($comment->comment_approved == '0') : ?>
                            <p><?php esc_html_e('Your comment is awaiting moderation.', 'kanter'); ?></p>
                        <?php endif; ?>

                        <?php comment_text(); ?>

                        <?php comment_reply_link(array_merge($args, array('depth' => $depth, 'max_depth' => $args['max_depth'], 'reply_text' => esc_html__('Reply', 'kanter')))); ?>

                        <?php edit_comment_link(esc_html__( 'Edit', 'kanter' ), '  ', ''); ?>

                    </div>
                </div>
            </div>
        <?php
    }
}

/****************************
    Portfolio Thumbnails Format
***************************/
if(!function_exists('kanter_portfolio_thumbnail_format')){
    function kanter_portfolio_thumbnail_format($size='') {

        global $post;


        $format = get_field('portfolio_type');
        $iframe_size = 'data-height='.get_field('portfolio_iframe_height').'';

        global $wp_embed;


        $output = '';

        if($format == 'photo_gallery'){
            if(class_exists('acf')) {

                $images = get_field('portfolio_gallery_images');

                $output .= '<div class="post-thumb">';
                $output .= '';
                $output .= '<div class="slider-wrap gallery-slide">';
                $output .= '<div class="slider-portfolio-single">';
                foreach ($images as $image) {
                    $output .= '<img src="' . $image['sizes'][$size] . '" alt="' . $image['alt'] . '">';
                }
                $output .= '</div>';
                $output .= '<div class="prev-next-block-rotate opacity-control al-control-portfolio-slider">
                                <div class="wrap-prev">
                                  <div class="prev"><i aria-hidden="true" class="fa fa-angle-left"></i></div>
                                </div>
                                <div class="wrap-next">
                                  <div class="next"><i aria-hidden="true" class="fa fa-angle-right"></i></div>
                                </div>
                              </div>
                            <!--Control sliders-->
                            <div class="dots-control-carousel al-dots-control"></div>';
                $output .= '</div>';
                $output .= '</div>';
            }
        }elseif($format == 'video_gallery'){
            if(class_exists('acf')) {

                $videos = get_field('p_video_gallery');

                $output .= '<div class="post-thumb">';
                $output .= '';
                $output .= '<div class="slider-wrap gallery-slide">';
                $output .= '<div class="slider-portfolio-single">';
                foreach ($videos as $video) {
                    $output .= '<div class="al-iframe-content" '.esc_attr($iframe_size).'>'.$wp_embed->run_shortcode( ' [embed]' . trim( $video['portfolio_video_gallery'] ) . '[/embed]' ).'</div>';
                }


                $output .= '</div>';
                $output .= '<div class="prev-next-block-rotate opacity-control al-control-portfolio-slider">
                                <div class="wrap-prev">
                                  <div class="prev"><i aria-hidden="true" class="fa fa-angle-left"></i></div>
                                </div>
                                <div class="wrap-next">
                                  <div class="next"><i aria-hidden="true" class="fa fa-angle-right"></i></div>
                                </div>
                              </div>
                            <!--Control sliders-->
                            <div class="al-dots-control dots-control-carousel"></div>';
                $output .= '</div>';
                $output .= '</div>';
            }
        }elseif($format == 'audio_gallery'){
            if(class_exists('acf')) {

                $audios = get_field('portfolio_audio_gallery');


                $output .= '<div class="post-thumb">';
                $output .= '<div class="slider-wrap gallery-slide">';
                $output .= '<div class="slider-portfolio-single">';
                foreach ($audios as $audio) {

                    $output .= '<div class="al-audio-gallery"> <p>'.$audio['portfolio_audio_gallery_repeat']['title'].'</p> '.do_shortcode('[audio src="'.$audio['portfolio_audio_gallery_repeat']['url'].'" ]').'</div>';
                }

                $output .= '</div>';
                $output .= '<div class="al-dots-control dots-control-carousel"></div>';
                $output .= '</div>';
                $output .= '</div>';
            }
        }elseif($format == 'audio'){
            if(class_exists('acf')):
                $type = get_field('portfolio_audio_type');
                $output .= '<div class="post-thumb">';
                $output .= '';
                switch ($type){
                    case 'link';
                        $audio = get_field('portfolio_audio_link');
                        $output .= '<div class="al-audio-gallery">'.do_shortcode('[audio src="'.$audio.'" ]').'</div>';
                        break;
                    case 'iframe';
                        $audio = get_field('portfolio_audio_iframe');
                        $output .= $audio;
                        break;
                    case 'iframe_url';
                        $audio = get_field('portfolio_audio_url');
                        $output .= '<div class="al-iframe-content" '.esc_attr($iframe_size).'>'.$wp_embed->run_shortcode( ' [embed]' . trim( $audio ) . '[/embed]' ).'</div>';
                        break;
                }
                $output .= '</div>';
            endif;

        }elseif($format == 'video'){
            $output .= '<div class="post-thumb">';
            $video = '<div class="al-iframe-content" '.esc_attr($iframe_size).'>'.$wp_embed->run_shortcode( ' [embed]' . trim( get_field('portfolio_video_iframe') ) . '[/embed]' ).'</div>';

            $output .= '<div class="embed-responsive">';
            $output .= $video;
            $output .= '</div>';
            $output .= '</div>';
        }elseif($format == 'default'){
            if(has_post_thumbnail()):
                $output .= '<div class="post-thumb">';
                $output .='<img src="'.kanter_get_thumbnail($post->ID, $size).'" alt="'.get_the_title().'" >';
                $output .= '</div>';
            endif;
        }else{
            if(has_post_thumbnail()):
                $output .= '<div class="post-thumb">';
                $output .='<img src="'.kanter_get_thumbnail($post->ID, $size).'" alt="'.get_the_title().'" >';
                $output .= '</div>';
            endif;
        }

        return $output;

    }
}

/****************************
    Type footer
***************************/
if(!function_exists('kanter_footer_type')) {
    function kanter_footer_type(){

        $output = false;

        if(class_exists('acf')){
            $output = get_field('page_type_footer');
        }

        if('' == $output || false == $output || 'none' == $output){
            $output = kanter_get_option('al-type-footer');
        }

        return $output;
    }
}

/****************************
Menu position
 ***************************/
if(!function_exists('kanter_menu_position')) {
    function kanter_menu_position(){

        $output = false;

        if(class_exists('acf')){
            $output = get_field('page_nav_position');
        }
        if('' == $output || false == $output || 'none' == $output){
            $output = kanter_get_option('al-menu-position');
        }

        return $output;
    }
}

/****************************
    Portfolio Navigation
***************************/
if(!function_exists('kanter_portfolio_navigation')) {
    function kanter_portfolio_navigation($before = '', $after = '') {


        $prevPost = get_previous_post();
        $nextPost = get_next_post();

        $output = '';

        if($prevPost || $nextPost):
            $output .= $before;

            $output .= '<div class="container">';

            if($prevPost){
                $output .= '<div class="col-md-6 al-left"><a class="al-previous-post" href="'.get_permalink($prevPost->ID).'">';
                $output .= '<span class="al-previous-title"><i class="fa fa-angle-left"></i>'.esc_html__('Previous Post', 'kanter').'</span>';
                $output .= get_the_title($prevPost->ID);
                $output .= '</div></a>';

            }

            if($nextPost){
                $output .= '<div class="col-md-6 al-right"><a class="al-next-post " href="'.get_permalink($nextPost->ID).'">';
                $output .= '<span class="al-next-title">'.esc_html__('Next Post', 'kanter').'<i class="fa fa-angle-right"></i></span>';
                $output .= get_the_title($nextPost->ID);
                $output .= '</div></a>';
            }


            $output .= '</div>';
            $output .= $after;
        endif;

        return $output;

    }
}

/****************************
    Password Protected
***************************/
add_filter( 'the_password_form', 'kanter_custom_password_form' );
if(!function_exists('kanter_custom_password_form')) {
    function kanter_custom_password_form() {
        global $post;
        $label = 'pwbox-'.( empty( $post->ID ) ? rand() : $post->ID );
        $output = '<div class="al-content-protected"><form action="' . esc_url( site_url( 'wp-login.php?action=postpass', 'login_post' ) ) . '" method="post">
    <p class="al-content-protected__description">' . esc_html__( 'To view this protected post, enter the password below:', 'kanter' ) . '</p><input name="post_password" id="' . $label . '" type="password" size="20" maxlength="20" placeholder="Password:" /><input class="btn" type="submit" name="Submit" value="' . esc_attr__( 'Submit', 'kanter' ) . '" /></form></div>
    ';
        return $output;
    }
}




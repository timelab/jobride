<?php
function kanter_import_files() {
    return array(
        array(
            'import_file_name'             => 'Kanter Demo Content',
            'local_import_file'            => trailingslashit( get_template_directory() ) . 'framework/includes/helper/demo-content/demo-content.xml',
            'local_import_widget_file'     => trailingslashit( get_template_directory() ) . 'framework/includes/helper/demo-content/widgets.wie',
            'local_import_customizer_file' => trailingslashit( get_template_directory() ) . 'framework/includes/helper/demo-content/customizer.dat',
        ),
    );
}
add_filter( 'pt-ocdi/import_files', 'kanter_import_files' );

function kanter_after_import_setup() {
    // Assign menus to their locations.
    $main_menu = get_term_by( 'name', 'Top nav', 'nav_menu' );
    $mobile_menu = get_term_by( 'name', 'Top nav', 'nav_menu' );
    $footer_menu = get_term_by( 'name', 'Pages', 'nav_menu' );

    print_r($main_menu);
    print_r($footer_menu);

    set_theme_mod( 'nav_menu_locations', array(
            'primary-menu' => $main_menu->term_id,
            'mobile-menu' => $mobile_menu->term_id,
            'footer-menu' => $footer_menu->term_id,
        )
    );

    // Assign front page and posts page (blog page).
    $front_page_id = get_page_by_title( 'Home' );
    $blog_page_id  = get_page_by_title( 'Blog – Sidebar' );

    update_option( 'show_on_front', 'page' );
    update_option( 'page_on_front', $front_page_id->ID );
    update_option( 'page_for_posts', $blog_page_id->ID );

}
add_action( 'pt-ocdi/after_import', 'kanter_after_import_setup' );
<?php

/****************************
    TGM Plugins Installer
***************************/
require_once(KANTER_REQUIRE_DIRECTORY . 'framework/includes/helper/class-tgm-plugin-activation.php');

/****************************
    Demo Content
***************************/
require_once(KANTER_REQUIRE_DIRECTORY . 'framework/includes/helper/demo-content/demo-content.php');

/****************************
    Ajax load
***************************/
require_once(KANTER_REQUIRE_DIRECTORY . 'framework/includes/helper/ajax-load/ajax-load.php');

/****************************
    Kirki
***************************/
require_once(KANTER_REQUIRE_DIRECTORY . 'framework/kirki/kirki.php');
require_once(KANTER_REQUIRE_DIRECTORY . 'framework/customizer.php');
